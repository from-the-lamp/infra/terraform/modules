variable "path" {
  type        = string
  default     = "kubernetes"
  description = "Path in Vault"
}
