variable "compartment_ocid" {
  type        = string
  description = "The OCID of the compartment where the resource will be created."
}

variable "username" {
  type        = string
  description = "The username associated with the resource."
}

variable "description" {
  type        = string
  description = "A description of the resource."
}
