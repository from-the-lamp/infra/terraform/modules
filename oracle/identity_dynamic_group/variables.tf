variable "compartment_ocid" {
  type = string
}

variable "name" {
  type    = string
  default = "cloud-controller-manager"
}
