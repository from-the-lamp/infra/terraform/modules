variable "gitlab_project_id" {
  type        = string
  description = "The ID of the GitLab project."
}

variable "gitlab_project_variable" {
  type        = string
  description = "The variable to be set for the GitLab project."
}
