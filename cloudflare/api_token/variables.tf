variable "cloudflare_zone_name" {
  type        = string
  description = "Cloudflare Zone name"
}

variable "cloudflare_token_name" {
  type        = string
  description = "Cloudflare Token name"
}
